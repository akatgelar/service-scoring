from SentimentEnglishTest import SentimentAnalysisEnglishTest
import requests
import json
import logging
from time import sleep
# import gspread
# from oauth2client.service_account import ServiceAccountCredentials

s = SentimentAnalysisEnglishTest(
# #     filename='SentiWordNet_3.0.0_20130122_English.txt', weighting='geometric')
# s = SentimentAnalysisIndonesia(
    filename='SentiWordNet_3.0.0_20130122_Indonesia.txt', weighting='geometric')
# URL = "https://translation.googleapis.com/language/translate/v2?key=AIzaSyCUvYYQJafpUYXyy_2qtuCnIcLdFLxFUPc"
# PARAMS = {"format": "text", "model": "nmt","source": "id", "target": "en", "q": []}
# HEADERS = {"content-type": "application/json"}

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M',
                    filename='log/access.log',
                    filemode='a+')
console = logging.StreamHandler()
console.setLevel(logging.INFO)
formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
console.setFormatter(formatter)
logging.getLogger('').addHandler(console)

logging.info('This is log \n')
logger1 = logging.getLogger('sentiment-analysis.service-translator')


def scoring(tweet):
    # try:
        # print(tweet)
    score = s.score(tweet)
    # except Exception as e:
    #     logger1.error("Error get score")
    #     logger1.error(e)

    return tweet 


def main():

    # string1 = 'You look beautiful wearing that dress'
    # score1 = scoring(string1)
    # print(score1)
    # string2 = 'He is a lawyer'
    # score2 = scoring(string2)
    # print(score2)
    # string3 = 'Thomas and I like to play with the kite'
    # score3 = scoring(string3)
    # print(score3)
    # string4 = 'We graduated from Senior High School last years.'
    # score4 = scoring(string4)
    # print(score4)
    # string5 = "She don't likes to drink orange juice"
    # score5 = scoring(string5)
    # print(score5)
    # string6 = 'Don’t forget to lock the door!'
    # score6 = scoring(string6)
    # print(score6)
    # string7 = 'Put your laptop in your desk.'
    # score7 = scoring(string7)
    # print(score7)
    # string8 = 'Stop playing games in your smartphone.'
    # score8 = scoring(string8)
    # print(score8)
    # string9 = 'Dad always tells me story when I wanted to go to sleep.'
    # score9 = scoring(string9)
    # print(score9)
    # string10 = 'Throw your rubish there.'
    # score10 = scoring(string10)
    # print(score10)
    # string11 = 'I got a headache from sleeping too much yesterday.'
    # score11 = scoring(string11)
    # print(score11)
    # string12 = 'I forgot to clean my bedroom.'
    # score12 = scoring(string12)
    # print(score12)
    # string13 = 'My mom and I went to buy some plants for our garden.'
    # score13 = scoring(string13)
    # print(score13)
    # string14 = 'My grand mother made me my favorite fried rice for lunch.'
    # score14 = scoring(string14)
    # print(score14)
    # string15 = 'I came late to my morning class but fortunately, my teacher lets me in.'
    # score15 = scoring(string15)
    # print(score15)
    # string16 = 'My dad came home from work soakinh wet.'
    # score16 = scoring(string16)
    # print(score16)
    # string17 = 'Adin laughed so hard at the joke that Afra told her.'
    # score17 = scoring(string17)
    # print(score17)
    # string18 = 'I’m confused when deciding what to eat for lunch.'
    # score18 = scoring(string18)
    # print(score18)
    # string19 = 'My dad loves to watch the updated news on tv everyday.'
    # score19 = scoring(string19)
    # print(score19)
    # string20 = 'I am buying fresh fruits with my mom.'
    # score20 = scoring(string20)
    # print(score20)
    # string21 = 'My father is fixing my brother’s broken bike.'
    # score21 = scoring(string21)
    # print(score21)
    # string22 = 'I say a greeting everytime I walk past my neighbours.'
    # score22 = scoring(string22)
    # print(score22)
    # string23 = 'Aku lupa menyapu rumah pagi ini.'
    # score23 = scoring(string23)
    # print(score23)
    # string24 = 'I wear hijab whenever I am going out.'
    # score24 = scoring(string24)
    # print(score24)
    # string25 = 'He had a haggard, worn apperance.'
    # score25 = scoring(string25)
    # print(score25)
    # string26 = 'Dad smacked me for taking back.'
    # score26 = scoring(string26)
    # print(score26)
    # string27 = 'there’s no hurry. Take your time'
    # score27 = scoring(string27)
    # print(score27)
    # string28 = 'She’s probably eating herself up inside.'
    # score28 = scoring(string28)
    # print(score28)
    # string29 = 'Why do you like asian people?'
    # score29 = scoring(string29)
    # print(score29)
    # string30 = 'She like to wear beautiful lacy dress.'
    # score30 = scoring(string30)
    # print(score30)
    # string31 = 'Mountain air feels cool and fresh'
    # score31 = scoring(string31)
    # print(score31)
    # string32 = 'Sometimes she’s very kind and friendly.'
    # score32 = scoring(string32)
    # print(score32)
    # string33 = 'I love banana'
    # score33 = scoring(string33)
    # print(score33)
    # string34 = 'The pants are broken'
    # score34 = scoring(string34)
    # print(score34)
    # string35 = 'It seems the construction is almost done'
    # score35 = scoring(string35)
    # print(score35)

    # string34 = 'The fruits are not safe to eat'
    string34 = 'Celana itu rusak'
    score34 = scoring(string34)
    print(string34)
    print(score34)
 

 


if __name__ == '__main__':
    main()
