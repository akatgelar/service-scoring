"""
Class to score sentiment of text.

Use domain-independent method of dictionary lookup of sentiment words,
handling negations and multiword expressions. Based on SentiWordNet 3.0.

"""

import nltk
import re


class SentimentAnalysisEnglishTest(object):
    """Class to get sentiment score based on analyzer."""
 
    def __init__(self, filename='SentiWordNet_3.0.0_20130122_Indonesia.txt', weighting='geometric'):
        # def __init__(self, filename='SentiWordNet_3.0.0_20130122_English.txt', weighting='geometric'):
        """Initialize with filename and choice of weighting."""
        if weighting not in ('geometric', 'harmonic', 'average'):
            raise ValueError(
                'Allowed weighting options are geometric, harmonic, average')
        # parse file and build sentiwordnet dicts
        self.swn_pos = {'a': {}, 'v': {}, 'r': {}, 'n': {}}
        self.swn_all = {}
        self.build_swn(filename, weighting)

    def average(self, score_list):
        """Get arithmetic average of scores."""
        if(score_list):
            return sum(score_list) / float(len(score_list))
        else:
            return 0

    def geometric_weighted(self, score_list):
        """"Get geometric weighted sum of scores."""
        weighted_sum = 0
        num = 1
        for el in score_list:
            weighted_sum += (el * (1 / float(2**num)))
            num += 1
        return weighted_sum

    # another possible weighting instead of average
    def harmonic_weighted(self, score_list):
        """Get harmonic weighted sum of scores."""
        weighted_sum = 0
        num = 2
        for el in score_list:
            weighted_sum += (el * (1 / float(num)))
            num += 1
        return weighted_sum

    def build_swn(self, filename, weighting):
        """Build class's lookup based on SentiWordNet 3.0."""
        records = [line.split('\t') for line in open(filename)]
        for rec in records:
            # has many words in 1 entry
            words = rec[4].split(", ")
            # words = rec[4].split()
            pos = rec[0]
            for word_num in words:
                word = word_num.split('#')[0]
                # print(word_num.split('#')[1])
                sense_num = int(word_num.split('#')[1])

                # build a dictionary key'ed by sense number
                if word not in self.swn_pos[pos]:
                    self.swn_pos[pos][word] = {}
                self.swn_pos[pos][word][sense_num] = float(
                    rec[2]) - float(rec[3])
                if word not in self.swn_all:
                    self.swn_all[word] = {}
                self.swn_all[word][sense_num] = float(rec[2]) - float(rec[3])

        # convert innermost dicts to ordered lists of scores
        for pos in self.swn_pos.keys():
            for word in self.swn_pos[pos].keys():
                newlist = [self.swn_pos[pos][word][k] for k in sorted(
                    self.swn_pos[pos][word].keys())]
                if weighting == 'average':
                    self.swn_pos[pos][word] = self.average(newlist)
                if weighting == 'geometric':
                    self.swn_pos[pos][word] = self.geometric_weighted(newlist)
                if weighting == 'harmonic':
                    self.swn_pos[pos][word] = self.harmonic_weighted(newlist)

        for word in self.swn_all.keys():
            newlist = [self.swn_all[word][k] for k in sorted(
                self.swn_all[word].keys())]
            if weighting == 'average':
                self.swn_all[word] = self.average(newlist)
            if weighting == 'geometric':
                self.swn_all[word] = self.geometric_weighted(newlist)
            if weighting == 'harmonic':
                self.swn_all[word] = self.harmonic_weighted(newlist)

    def pos_short(self, pos):
        """Convert NLTK POS tags to SWN's POS tags."""
        if pos in set(['VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ']):
            return 'v'
        elif pos in set(['JJ', 'JJR', 'JJS']):
            return 'a'
        elif pos in set(['RB', 'RBR', 'RBS']):
            return 'r'
        elif pos in set(['NNS', 'NN', 'NNP', 'NNPS']):
            return 'n'
        else:
            return 'a'

    def score_word(self, word, pos):
        """Get sentiment score of word based on SWN and part of speech."""
        try:
            return self.swn_pos[pos][word]
        except KeyError:
            try:
                return self.swn_all[word]
            except KeyError:
                return 0

    def score(self, sentence):
        """Sentiment score a sentence."""
        # init sentiwordnet lookup/scoring tools
        impt = set(['NNS', 'NN', 'NNP', 'NNPS', 'JJ', 'JJR', 'JJS',
                    'RB', 'RBR', 'RBS', 'VB', 'VBD', 'VBG', 'VBN',
                    'VBP', 'VBZ', 'unknown'])
        non_base = set(['VBD', 'VBG', 'VBN', 'VBP', 'VBZ', 'NNS', 'NNPS'])
        # negations = set(['not', 'n\'t', 'less', 'no', 'never',
        #                  'nothing', 'nowhere', 'hardly', 'barely',
        #                  'scarcely', 'nobody', 'none'])
        negations = set(['tidak', 'tak', 'tiada', 'jangan', 'bukan',
                         'belum', 'salah'])

        # stopwords = nltk.corpus.stopwords.words('english')
        stopwords = nltk.corpus.stopwords.words('indonesian')
        print("stopwords")
        print(stopwords)
        
        wnl = nltk.WordNetLemmatizer()

        scores = []
        tokens = nltk.tokenize.word_tokenize(sentence)
        print("tokens")
        print(tokens)

        tagged = nltk.pos_tag(tokens, "", "eng")
        print("tagged")
        print(tagged)

        index = 0
        for el in tagged:

            pos = el[1]
            try:
                word = re.match('(\w+)', el[0]).group(0).lower()
                print("")
                print("\t"+"word")
                print("\t"+word)

                start = index - 5
                if start < 0:
                    start = 0
                neighborhood = tokens[start:index]
                # print("\t"+"neighborhood")
                # print("\t"+ str(neighborhood))

                # look for trailing multiword expressions
                word_minus_one = tokens[index-1:index+1]
                # print("\t"+"word_minus_one")
                # print("\t"+str(word_minus_one))

                word_minus_two = tokens[index-2:index+1]
                # print("\t"+"word_minus_two")
                # print("\t"+str(word_minus_two))

                # if multiword expression, fold to one expression
                if(self.is_multiword(word_minus_two)):
                    if len(scores) > 1:
                        scores.pop()
                        scores.pop()
                    if len(neighborhood) > 1:
                        neighborhood.pop()
                        neighborhood.pop()
                    word = '_'.join(word_minus_two)
                    print("\t"+"multiword")
                    print("\t"+str(word))
                    pos = 'unknown'

                elif(self.is_multiword(word_minus_one)):
                    if len(scores) > 0:
                        scores.pop()
                    if len(neighborhood) > 0:
                        neighborhood.pop()
                    word = '_'.join(word_minus_one)
                    print("\t"+"multiword")
                    print("\t"+str(word))
                    pos = 'unknown'

                # perform lookup
                if (pos in impt) and (word not in stopwords):
                    if pos in non_base:
                        word = wnl.lemmatize(word, self.pos_short(pos))
                        print("\t"+"\t"+"wordnet")
                        print("\t"+"\t"+word)
                    score = self.score_word(word, self.pos_short(pos))
                    print("\t"+"\t"+"score")
                    print("\t"+"\t"+str(score))
                    if len(negations.intersection(set(neighborhood))) > 0:
                        print("\t"+"\t"+"negation")
                        print("\t"+"\t"+neighborhood[len(neighborhood)-1]+ " " + word)
                        print("\t"+"\t"+"score")
                        score = -score
                        print("\t"+"\t"+str(score))
                    scores.append(score)

            except AttributeError:
                pass

            index += 1

        if len(scores) > 0:
            print()
            print("\t"+"\t"+"\t"+"scores")
            print("\t"+"\t"+"\t"+str(scores))
            print("\t"+"\t"+"\t"+str(sum(scores)))
            print("\t"+"\t"+"\t"+str(float(len(scores))))
            print("\t"+"\t"+"\t"+str(sum(scores) / float(len(scores))))
            return sum(scores) / float(len(scores))
        else:
            return 0

    def is_multiword(self, words):
        """Test if a group of words is a multiword expression."""
        joined = '_'.join(words)
        return joined in self.swn_all
