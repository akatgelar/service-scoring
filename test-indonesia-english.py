from SentimentEnglish import SentimentAnalysisEnglish
import requests
import json
import logging
from time import sleep
import gspread
from oauth2client.service_account import ServiceAccountCredentials

s = SentimentAnalysisEnglish(
    filename='SentiWordNet_3.0.0_20130122_English.txt', weighting='geometric')
# URL = "https://translation.googleapis.com/language/translate/v2?key=AIzaSyCUvYYQJafpUYXyy_2qtuCnIcLdFLxFUPc"
# PARAMS = {"format": "text", "model": "nmt","source": "id", "target": "en", "q": []}
# HEADERS = {"content-type": "application/json"}

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M',
                    filename='log/access.log',
                    filemode='a+')
console = logging.StreamHandler()
console.setLevel(logging.INFO)
formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
console.setFormatter(formatter)
logging.getLogger('').addHandler(console)

logging.info('This is log \n')
logger1 = logging.getLogger('sentiment-analysis.service-translator')
 
def scoring(tweet, gc):
    # PARAMS["q"] = tweet
    sleep(3)
    try:
        
        # wks = gc.open("service-translator-sentiment-analysis-akatgelar").sheet1
        wks = gc.open("service-translator-sentiment-analysis-indramaulana").sheet1
        # wks = gc.open("service-translator-sentiment-analysis-1").sheet1
        # wks = gc.open("service-translator-sentiment-analysis-2").sheet1
        wks.update_acell('A2', '=gTranslate("'+tweet+'", "id", "en")')
        cell_list = wks.acell('A2').value

        try:
            # print(tweet)
            # print(cell_list)
            score = s.score(cell_list)
            print(score)
        except Exception as e:
            logger1.error("Error get score")
            logger1.error(e)

    except Exception as e:
        logger1.error("Error POST to Google Translate")
        logger1.error(e)

    return score


def main():

    # translate = requests.post(url=URL, params=PARAMS, headers=HEADERS, timeout=10)
    # response_translate = translate.json()

    scope = ['https://spreadsheets.google.com/feeds','https://www.googleapis.com/auth/drive']
    # credentials = ServiceAccountCredentials.from_json_keyfile_name('client_secret_akatgelar.json', scope)
    credentials = ServiceAccountCredentials.from_json_keyfile_name('client_secret_indramaulana.json', scope)
    # credentials = ServiceAccountCredentials.from_json_keyfile_name('client_secret_sentimentanalysis1.json', scope)
    # credentials = ServiceAccountCredentials.from_json_keyfile_name('client_secret_sentimentanalysis2.json', scope)

    gc = gspread.authorize(credentials)

     

    string1 = 'Kamu terlihat cantik memakai baju itu'
    score1 = scoring(string1, gc)
    string2 = 'Dia adalah seorang pengacara'
    score2 = scoring(string2, gc)
    string3 = 'Aku dan Thomas suka bermai layang-layang'
    score3 = scoring(string3, gc)
    string4 = 'Kita lulus SMA tahun lalu'
    score4 = scoring(string4, gc)
    string5 = 'Dia tidak suka meminum jus jeruk'
    score5 = scoring(string5, gc)
    string6 = 'Jangan lupa mengunci pintu!'
    score6 = scoring(string6, gc)
    string7 = 'Taruh laptopmu di meja.'
    score7 = scoring(string7, gc)
    string8 = 'Berhenti bermain game di hpmu.'
    score8 = scoring(string8, gc)
    string9 = 'Ayah selalu bercerita saat aku ingin tidur.'
    score9 = scoring(string9, gc)
    string10 = 'Buang sampahmu disana.'
    score10 = scoring(string10, gc)
    string11 = 'Aku sakit kepala karena terlalu banyak tidur kemarin.'
    score11 = scoring(string11, gc)
    string12 = 'Aku lupa membersihkan kamarku.'
    score12 = scoring(string12, gc)
    string13 = 'Ibuku dan aku pergi membeli beberapa tanaman untuk kebun kami.'
    score13 = scoring(string13, gc)
    string14 = 'Nenekku membuatkanku nasi goreng favoritku untuk makan siang.'
    score14 = scoring(string14, gc)
    string15 = 'Aku datang terlambat ke kelas pagiku, tapi untungnya guruku mengizinkanku masuk.'
    score15 = scoring(string15, gc)
    string16 = 'Ayahku pulang ke rumah dari pergi kerja dalam keadaan basah kuyup.'
    score16 = scoring(string16, gc)
    string17 = 'Adin tertawa terbahak bahak karena lelucon yang Afra'
    score17 = scoring(string17, gc)
    string18 = 'Aku bingung saat memutuskan apa yang akan kumakan untuk makan siang.'
    score18 = scoring(string18, gc)
    string19 = 'Ayahku suka melihat berita terbaru di TV setiap hari.'
    score19 = scoring(string19, gc)
    string20 = 'Aku sedang membeli buah segar dengan ibuku.'
    score20 = scoring(string20, gc)
    string21 = 'Ayahku sedang memperbaiki sepeda kakakku yang rusak.'
    score21 = scoring(string21, gc)
    string22 = 'Aku menyapa tetanggaku  setiap kali aku berjalan melewati rumahnya'
    score22 = scoring(string22, gc)
    string23 = 'I wash my face right after I woke up.'
    score23 = scoring(string23, gc)
    string24 = 'Aku memakai kerudung setiap aku akan pergi'
    score24 = scoring(string24, gc)
    string25 = 'Dia berpenampilan kuyu dan lesu.'
    score25 = scoring(string25, gc)
    string26 = 'Ayah memukul ku karena membantah'
    score26 = scoring(string26, gc)
    string27 = 'tidak perlu terburu-buru. Santai saja.'
    score27 = scoring(string27, gc)
    string28 = 'Dia mungkin menahan kesedihannya'
    score28 = scoring(string28, gc)
    string29 = 'Kenapa kamu suka orang asia?'
    score29 = scoring(string29, gc)
    string30 = 'Dia senang memakai gaun ondah yang berenda'
    score30 = scoring(string30, gc)
    string31 = 'Udara gunung terasa sejuk dan segar'
    score31 = scoring(string31, gc)
    string32 = 'Terkadang dia sangat baik dan ramah'
    score32 = scoring(string32, gc)
    string33 = 'Aku suka pisang'
    score33 = scoring(string33, gc)
    string34 = 'Celana itu rusak'
    score34 = scoring(string34, gc)
    string35 = 'Sepertinya bangunan itu hampir beres'
    score35 = scoring(string35, gc)



 

 


if __name__ == '__main__':
    main()
