from SentimentIndonesia import SentimentAnalysisIndonesia
import requests
import json
import logging

s = SentimentAnalysisIndonesia(
    filename='SentiWordNet_3.0.0_20130122_Indonesia.txt', weighting='geometric')

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M',
                    filename='log/access.log',
                    filemode='a+')
console = logging.StreamHandler()
console.setLevel(logging.INFO)
formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
console.setFormatter(formatter)
logging.getLogger('').addHandler(console)

logging.info('This is log \n')
logger1 = logging.getLogger('sentiment-analysis.service-translator')


def scoring(tweet):

    try:
        # print(tweet)
        score = s.score(tweet)
    except Exception as e:
        logger1.error("Error get score")
        logger1.error(e)

    return score


def main():
    
    string1 = 'bagus'
    score1 = scoring(string1)
    print(score1)
    # string1 = 'Kamu terlihat cantik memakai baju itu'
    # score1 = scoring(string1)
    # print(score1)
    # string2 = 'Dia adalah seorang pengacara'
    # score2 = scoring(string2)
    # print(score2)
    # string3 = 'Aku dan Thomas suka bermai layang-layang'
    # score3 = scoring(string3)
    # print(score3)
    # string4 = 'Kita lulus SMA tahun lalu'
    # score4 = scoring(string4)
    # print(score4)
    # string5 = 'Dia tidak suka meminum jus jeruk'
    # score5 = scoring(string5)
    # print(score5)
    # string6 = 'Jangan lupa mengunci pintu!'
    # score6 = scoring(string6)
    # print(score6)
    # string7 = 'Taruh laptopmu di meja.'
    # score7 = scoring(string7)
    # print(score7)
    # string8 = 'Berhenti bermain game di hpmu.'
    # score8 = scoring(string8)
    # print(score8)
    # string9 = 'Ayah selalu bercerita saat aku ingin tidur.'
    # score9 = scoring(string9)
    # print(score9)
    # string10 = 'Buang sampahmu disana.'
    # score10 = scoring(string10)
    # print(score10)
    # string11 = 'Aku sakit kepala karena terlalu banyak tidur kemarin.'
    # score11 = scoring(string11)
    # print(score11)
    # string12 = 'Aku lupa membersihkan kamarku.'
    # score12 = scoring(string12)
    # print(score12)
    # string13 = 'Ibuku dan aku pergi membeli beberapa tanaman untuk kebun kami.'
    # score13 = scoring(string13)
    # print(score13)
    # string14 = 'Nenekku membuatkanku nasi goreng favoritku untuk makan siang.'
    # score14 = scoring(string14)
    # print(score14)
    # string15 = 'Aku datang terlambat ke kelas pagiku, tapi untungnya guruku mengizinkanku masuk.'
    # score15 = scoring(string15)
    # print(score15)
    # string16 = 'Ayahku pulang ke rumah dari pergi kerja dalam keadaan basah kuyup.'
    # score16 = scoring(string16)
    # print(score16)
    # string17 = 'Adin tertawa terbahak bahak karena lelucon yang Afra'
    # score17 = scoring(string17)
    # print(score17)
    # string18 = 'Aku bingung saat memutuskan apa yang akan kumakan untuk makan siang.'
    # score18 = scoring(string18)
    # print(score18)
    # string19 = 'Ayahku suka melihat berita terbaru di TV setiap hari.'
    # score19 = scoring(string19)
    # print(score19)
    # string20 = 'Aku sedang membeli buah segar dengan ibuku.'
    # score20 = scoring(string20)
    # print(score20)
    # string21 = 'Ayahku sedang memperbaiki sepeda kakakku yang rusak.'
    # score21 = scoring(string21)
    # print(score21)
    # string22 = 'Aku menyapa tetanggaku  setiap kali aku berjalan melewati rumahnya'
    # score22 = scoring(string22)
    # print(score22)
    # string23 = 'I wash my face right after I woke up.'
    # score23 = scoring(string23)
    # print(score23)
    # string24 = 'Aku memakai kerudung setiap aku akan pergi'
    # score24 = scoring(string24)
    # print(score24)
    # string25 = 'Dia berpenampilan kuyu dan lesu.'
    # score25 = scoring(string25)
    # print(score25)
    # string26 = 'Ayah memukul ku karena membantah'
    # score26 = scoring(string26)
    # print(score26)
    # string27 = 'tidak perlu terburu-buru. Santai saja.'
    # score27 = scoring(string27)
    # print(score27)
    # string28 = 'Dia mungkin menahan kesedihannya'
    # score28 = scoring(string28)
    # print(score28)
    # string29 = 'Kenapa kamu suka orang asia?'
    # score29 = scoring(string29)
    # print(score29)
    # string30 = 'Dia senang memakai gaun ondah yang berenda'
    # score30 = scoring(string30)
    # print(score30)
    # string31 = 'Udara gunung terasa sejuk dan segar'
    # score31 = scoring(string31)
    # print(score31)
    # string32 = 'Terkadang dia sangat baik dan ramah'
    # score32 = scoring(string32)
    # print(score32)
    # string33 = 'Aku suka pisang'
    # score33 = scoring(string33)
    # print(score33)
    # string34 = 'Celana itu rusak'
    # score34 = scoring(string34)
    # print(score34)
    # string35 = 'Sepertinya bangunan itu hampir beres'
    # score35 = scoring(string35)
    # print(score35)



if __name__ == '__main__':
    main()
